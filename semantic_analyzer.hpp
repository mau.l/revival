#ifndef SEMANTIC_ANALYZER_HPP
#define SEMANTIC_ANALYZER_HPP
#include "parsr.hpp"

struct SEMANTIC_ANALYZER : public VISITOR {
private:
   std::map<vec<id_t*>, e_t*> func;
   vec<id_t*>* id_stack{ nullptr };
public :
   void visit(tok   *p)override {  }
   void visit(id_t  *p)override { if (id_stack != nullptr) id_stack->push_back(p); }
   void visit(str_t *p)override {  }
   void visit(i32_t *p)override {  }
   void visit(i64_t *p)override {  }
   void visit(i128_t*p)override {  }
   void visit(f32_t *p)override {  }
   void visit(f64_t *p)override {  }

   void visit(body_t*p)override { for (auto t : p->ts) t->let_in(this); }
   void visit(seq_t *p)override { for (auto t : p->ts) t->let_in(this); }

   void visit(mor_eq_t*p)override {  }
   void visit(les_eq_t*p)override {  }
   void visit(He_eq_t *p)override {  }
   void visit(dot_t   *p)override {  }
   void visit(asg_t   *p)override {  }
   void visit(add_t   *p)override {  }
   void visit(sub_t   *p)override {  }
   void visit(mul_t   *p)override {  }
   void visit(sep_t   *p)override {  }
   void visit(mor_t   *p)override {  }
   void visit(les_t   *p)override {  }
   void visit(eq_t    *p)override {  }
   void visit(is_t    *p)override {  }

   void visit(u_sub_t *p)override {  }
   void visit(He_t    *p)override {  }

   void visit(for_i_t *p)override {  }
   void visit(for_t   *p)override {  }

   void visit(npu_t *p)override {  }
   void visit(lam_t *p)override {  }

   void visit(forch_i_t  *p)override {  }
   void visit(forch_x_t  *p)override {  }
   void visit(forch_i_c_t*p)override {  }
   void visit(forch_x_c_t*p)override {  }

   void visit(cond_t *p)override {  }
   void visit(call_t *p)override {  }
   void visit(fun_t  *p)override
   {
      vec<id_t*> id;
      id_stack = &id;
      //p->id_->let_in(this);
      id_stack = nullptr;
      func[id] = p->b;
   }
};

#endif // SEMANTIC_ANALYZER_HPP
