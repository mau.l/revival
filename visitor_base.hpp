#ifndef VISITOR_BASE_HPP
#define VISITOR_BASE_HPP

struct VISITOR {
   virtual void visit(struct tok   *) = 0;
   virtual void visit(struct id_t  *) = 0;
   virtual void visit(struct str_t *) = 0;
   virtual void visit(struct i32_t *) = 0;
   virtual void visit(struct i64_t *) = 0;
   virtual void visit(struct i128_t*) = 0;
   virtual void visit(struct f32_t *) = 0;
   virtual void visit(struct f64_t *) = 0;

   virtual void visit(struct body_t*) = 0;
   virtual void visit(struct seq_t *) = 0;

   virtual void visit(struct mor_eq_t*) = 0;
   virtual void visit(struct les_eq_t*) = 0;
   virtual void visit(struct He_eq_t *) = 0;
   virtual void visit(struct dot_t   *) = 0;
   virtual void visit(struct asg_t   *) = 0;
   virtual void visit(struct add_t   *) = 0;
   virtual void visit(struct sub_t   *) = 0;
   virtual void visit(struct mul_t   *) = 0;
   virtual void visit(struct sep_t   *) = 0;
   virtual void visit(struct mor_t   *) = 0;
   virtual void visit(struct les_t   *) = 0;
   virtual void visit(struct eq_t    *) = 0;
   virtual void visit(struct is_t    *) = 0;

   virtual void visit(struct u_sub_t *) = 0;
   virtual void visit(struct He_t    *) = 0;

   virtual void visit(struct for_i_t *) = 0;
   virtual void visit(struct for_t   *) = 0;

   virtual void visit(struct npu_t *) = 0;
   virtual void visit(struct lam_t *) = 0;

   virtual void visit(struct forch_i_t   *) = 0;
   virtual void visit(struct forch_x_t   *) = 0;
   virtual void visit(struct forch_i_c_t *) = 0;
   virtual void visit(struct forch_x_c_t *) = 0;

   virtual void visit(struct cond_t *) = 0;
   virtual void visit(struct call_t *) = 0;
   virtual void visit(struct fun_t  *) = 0;
};


#endif // VISITOR_BASE_HPP
