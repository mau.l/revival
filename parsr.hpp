#ifndef PARSR_HPP
#define PARSR_HPP
#include "lexr.hpp"
#include "visitor_base.hpp"

fn def_cb (vec<tok*> &sub, vec<tok*> &s) -> void
{
   std::cout << "ERROR!" << std::endl;
}


struct RULE {
   vec<tk> s;
   int power{ -1 };
   fn (*cb)(vec<tok*>&, vec<tok*>&) -> void = def_cb;
   vec<tk> out{ tk::e };
};


struct e_t    : public tok { e_t   () { tok::type  = tk::e;    } ~e_t   () {} tk type2{ tk::e }; };


struct id_t   : public e_t { id_t  () { e_t::type2 = tk::id;   } ~id_t  () {} id  *p; void let_in(VISITOR*v)override{v->visit(this);} };
struct str_t  : public e_t { str_t () { e_t::type2 = tk::str;  } ~str_t () {} str *p; void let_in(VISITOR*v)override{v->visit(this);} };
struct i32_t  : public e_t { i32_t () { e_t::type2 = tk::i32;  } ~i32_t () {} i32 *p; void let_in(VISITOR*v)override{v->visit(this);} };
struct i64_t  : public e_t { i64_t () { e_t::type2 = tk::i64;  } ~i64_t () {} i64 *p; void let_in(VISITOR*v)override{v->visit(this);} };
struct i128_t : public e_t { i128_t() { e_t::type2 = tk::i128; } ~i128_t() {} i128*p; void let_in(VISITOR*v)override{v->visit(this);} };
struct f32_t  : public e_t { f32_t () { e_t::type2 = tk::f32;  } ~f32_t () {} f32 *p; void let_in(VISITOR*v)override{v->visit(this);} };
struct f64_t  : public e_t { f64_t () { e_t::type2 = tk::f64;  } ~f64_t () {} f64 *p; void let_in(VISITOR*v)override{v->visit(this);} };
template <typename T, typename Y> fn cb_one(vec<tok*> &sub, vec<tok*> &s) -> void
{
   Y* t = add_tok(new Y());
   t->p = reinterpret_cast<T*>(sub[0]);
   t->line = sub[0]->line;
   t->type2 = sub[0]->type;
   s.push_back(reinterpret_cast<tok*>(t));
}
struct str_tp_t  : public e_t { str_tp_t  () { e_t::type2 = tk::str_tp ; } ~str_tp_t () {} void let_in(VISITOR*v)override { v->visit(this); } };
struct i32_tp_t  : public e_t { i32_tp_t  () { e_t::type2 = tk::i32_tp ; } ~i32_tp_t () {} void let_in(VISITOR*v)override { v->visit(this); } };
struct i64_tp_t  : public e_t { i64_tp_t  () { e_t::type2 = tk::i64_tp ; } ~i64_tp_t () {} void let_in(VISITOR*v)override { v->visit(this); } };
struct i128_tp_t : public e_t { i128_tp_t () { e_t::type2 = tk::i128_tp; } ~i128_tp_t() {} void let_in(VISITOR*v)override { v->visit(this); } };
struct f32_tp_t  : public e_t { f32_tp_t  () { e_t::type2 = tk::f32_tp ; } ~f32_tp_t () {} void let_in(VISITOR*v)override { v->visit(this); } };
struct f64_tp_t  : public e_t { f64_tp_t  () { e_t::type2 = tk::f64_tp ; } ~f64_tp_t () {} void let_in(VISITOR*v)override { v->visit(this); } };
template <typename T> fn cb_one_tp(vec<tok*> &_, vec<tok*> &s) -> void
{
   T* t = add_tok(new T());
   t->line = _[0]->line;
   s.push_back(reinterpret_cast<tok*>(t));
}

static std::map<std::string, e_t*> TYPES_POOL;
static std::vector<id_t*> CURR_PTH;

fn init_std_types() -> void
{
   for (auto& id_ : base_types_id_s) {
      switch (id_.second) {
         case tk:: str_tp: { TYPES_POOL[id_.first] = add_tok(new  str_tp_t()); break; }
         case tk:: i32_tp: { TYPES_POOL[id_.first] = add_tok(new  i32_tp_t()); break; }
         case tk:: i64_tp: { TYPES_POOL[id_.first] = add_tok(new  i64_tp_t()); break; }
         case tk::i128_tp: { TYPES_POOL[id_.first] = add_tok(new i128_tp_t()); break; }
         case tk:: f32_tp: { TYPES_POOL[id_.first] = add_tok(new  f32_tp_t()); break; }
         case tk:: f64_tp: { TYPES_POOL[id_.first] = add_tok(new  f64_tp_t()); break; }
         default: break;
      }
   }
}


struct cnt_t : public e_t { void let_in(VISITOR*v)override { v->visit(this); } };
struct br_t  : public e_t { void let_in(VISITOR*v)override { v->visit(this); } };
template <typename T> fn cb_atom(vec<tok*> &sub, vec<tok*> &s) -> void
{
   T* t = add_tok(new T());
   t->line = sub[0]->line;
   t->type2 = sub[0]->type;
   s.push_back(reinterpret_cast<tok*>(t));
}

struct body_t: public e_t { vec<e_t*> ts; void let_in(VISITOR*v)override{v->visit(this);} };
struct  seq_t: public e_t { vec<e_t*> ts; void let_in(VISITOR*v)override{v->visit(this);} };
template <typename T, tk Y> fn cb_seq(vec<tok*> &sub, vec<tok*> &s) -> void // e , e // e\n e
{
   e_t* e1 = reinterpret_cast<e_t*>(sub[0]);
   T* sq;
   if (e1->type2 == Y) {
      sq = reinterpret_cast<T*>(e1);
   }
   else {
      (sq = add_tok(new T()))->ts.push_back(reinterpret_cast<e_t*>(sub[0]));
      sq->line = sub[0]->line;
      sq->type2 = Y;
   }
   sq->ts.push_back(reinterpret_cast<e_t*>(sub[2]));
   s.push_back(sq);
}


struct mor_eq_t : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct les_eq_t : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct He_eq_t  : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct dot_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct asg_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct add_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct sub_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct mul_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct sep_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct mor_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct les_t    : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct eq_t     : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct is_t     : public e_t { e_t *l, *r; void let_in(VISITOR*v)override{v->visit(this);} };
template <typename T, tk Y> fn cb_bOp(vec<tok*> &sub, vec<tok*> &s) -> void
{
   T* t = add_tok(new T());
   t->type2 = Y;
   t->l = reinterpret_cast<e_t*>(sub[0]);
   t->r = reinterpret_cast<e_t*>(sub[2]);
   t->line = sub[1]->line;
   s.push_back(t);
}
struct u_sub_t : public e_t { e_t *r; void let_in(VISITOR*v)override{v->visit(this);} };
struct    He_t : public e_t { e_t *r; void let_in(VISITOR*v)override{v->visit(this);} };
template <typename T, tk Y> fn cb_uOp(vec<tok*> &sub, vec<tok*> &s) -> void
{
   T* t = add_tok(new T());
   t->type2 = Y;
   t->r = reinterpret_cast<e_t*>(sub[1]);
   t->line = sub[0]->line;
   s.push_back(t);
}
fn cb_u_add(vec<tok*> &sub, vec<tok*> &s) -> void
{
   s.push_back(sub[1]);
}


struct for_i_t : public e_t { id_t *i, *x; e_t *l, *b; void let_in(VISITOR*v)override{v->visit(this);} };
struct for_t   : public e_t { id_t     *x; e_t *l, *b; void let_in(VISITOR*v)override{v->visit(this);} };
template <size_t i, size_t x, size_t l, size_t b> fn cb_for(vec<tok*> &sub, vec<tok*> &s) -> void
{
   e_t* t;
   if (i) {
      for_i_t *p = add_tok(new for_i_t());
      p->type2 = tk::for_i;
      p->i = reinterpret_cast<id_t*>(sub[i]);
      p->x = reinterpret_cast<id_t*>(sub[x]);
      p->l = reinterpret_cast<e_t*>(sub[l]);
      p->b = reinterpret_cast<e_t*>(sub[b]);
      t = p;
   }
   else {
      for_t *p = add_tok(new for_t());
      p->type2 = tk::for_x;
      p->x = reinterpret_cast<id_t*>(sub[x]);
      p->l = reinterpret_cast<e_t*>(sub[l]);
      p->b = reinterpret_cast<e_t*>(sub[b]);
      t = p;
   }
   t->line = sub[0]->line;
   s.push_back(t);
}


struct npu_t : public e_t { e_t *c{ nullptr }, *b, *el{ nullptr }; void let_in(VISITOR*v)override{v->visit(this);} };
template <size_t c, size_t b, size_t el> fn cb_npu(vec<tok*> &sub, vec<tok*> &s) -> void
{
   npu_t* t = add_tok(new npu_t());
   t->type = sub[0]->type == tk::npu ? tk::e : tk::uHa_;
   t->type2 = c && el ? tk::uHanpu : c ? tk::npu : tk::uHa;
   t->line = sub[0]->line;
   if (c ) t->c = reinterpret_cast<e_t*>(sub[c] );
   if (b ) t->b = reinterpret_cast<e_t*>(sub[b] );
   if (el) t->el= reinterpret_cast<e_t*>(sub[el]);
   s.push_back(t);
}
fn cb_par(vec<tok*> &sub, vec<tok*> &s) -> void
{
   s.push_back(sub[1]);
}
fn cb_empty_par(vec<tok*> &sub, vec<tok*> &s) -> void
{
   s.push_back(sub[0]);
   seq_t *sq;
   s.push_back(add_tok(sq = new seq_t()));
   sq->type2 = tk::seq;
   s.push_back(sub[1]);
}


struct whl_t : public e_t { e_t *c{ nullptr }, *b{ nullptr }; void let_in(VISITOR*v)override { v->visit(this); } };
fn cb_whl(vec<tok*> &sub, vec<tok*> &s) -> void
{
   npu_t* t = add_tok(new npu_t());
   t->type2 = tk::whl;
   t->line = sub[0]->line;
   t->c = reinterpret_cast<e_t*>(sub[1]);
   t->b = reinterpret_cast<e_t*>(sub[3]);
   s.push_back(t);
}


struct lam_t : public e_t { e_t *args; e_t *e; void let_in(VISITOR*v)override{v->visit(this);} };
fn cb_lam(vec<tok*>& sub, vec<tok*>& s) -> void
{
   lam_t* t = add_tok(new lam_t());
   t->type2 = tk::lam;
   t->args  = reinterpret_cast<e_t*>(sub[2]);
   t->e     = reinterpret_cast<e_t*>(sub[5]);
   t->line = sub[0]->line;
   s.push_back(t);
}


struct forch_i_t : public e_t { id_t *i, *x; e_t *l, *b; void let_in(VISITOR*v)override{v->visit(this);} };
struct forch_x_t : public e_t { id_t     *x; e_t *l, *b; void let_in(VISITOR*v)override{v->visit(this);} };
struct forch_i_c_t : public e_t { id_t *i, *x; e_t *l, *b, *c; void let_in(VISITOR*v)override{v->visit(this);} };
struct forch_x_c_t : public e_t { id_t     *x; e_t *l, *b, *c; void let_in(VISITOR*v)override{v->visit(this);} };
template <size_t i, size_t x, size_t l, size_t b, size_t c> fn cb_forch(vec<tok*> &sub, vec<tok*> &s) -> void
{
   e_t *t;
   if (c) {
      if (i) {
         forch_i_c_t* p = add_tok(new forch_i_c_t());
         p->type2 = tk::forch_i_c;
         p->i = reinterpret_cast<id_t*>(sub[i]);
         p->x = reinterpret_cast<id_t*>(sub[x]);
         p->l = reinterpret_cast<e_t*>(sub[l]);
         p->b = reinterpret_cast<e_t*>(sub[b]);
         p->c = reinterpret_cast<e_t*>(sub[c]);
         t = p;
      }
      else {
         forch_x_c_t* p = add_tok(new forch_x_c_t());
         p->type2 = tk::forch_x_c;
         p->x = reinterpret_cast<id_t*>(sub[x]);
         p->l = reinterpret_cast<e_t*>(sub[l]);
         p->b = reinterpret_cast<e_t*>(sub[b]);
         p->c = reinterpret_cast<e_t*>(sub[c]);
         t = p;
      }
   }
   else {
      if (i) {
         forch_i_t* p = add_tok(new forch_i_t());
         p->type2 = tk::forch_i;
         p->i = reinterpret_cast<id_t*>(sub[i]);
         p->x = reinterpret_cast<id_t*>(sub[x]);
         p->l = reinterpret_cast<e_t*>(sub[l]);
         p->b = reinterpret_cast<e_t*>(sub[b]);
         t = p;
      }
      else {
         forch_x_t* p = add_tok(new forch_x_t());
         p->type2 = tk::forch_x;
         p->x = reinterpret_cast<id_t*>(sub[x]);
         p->l = reinterpret_cast<e_t*>(sub[l]);
         p->b = reinterpret_cast<e_t*>(sub[b]);
         t = p;
      }
   }
   t->line = sub[0]->line;
   s.push_back(t);
}


struct cond_t : public e_t { e_t *c; e_t *tr, *fl; void let_in(VISITOR*v)override{v->visit(this);} };
fn cb_cond(vec<tok*>& sub, vec<tok*>& s) -> void
{
   cond_t *t = add_tok(new cond_t());
   t->type2 = tk::cond;
   t->c  = reinterpret_cast<e_t*>(sub[2]);
   t->tr = reinterpret_cast<e_t*>(sub[0]);
   t->fl = reinterpret_cast<e_t*>(sub[4]);
   t->line = sub[0]->line;
   s.push_back(t);
}


struct call_t : public e_t { e_t* id; e_t* args; void let_in(VISITOR*v)override{v->visit(this);} };
fn cb_call(vec<tok*> &sub, vec<tok*> &s) -> void
{
   call_t* t = add_tok(new call_t());
   t->id   = reinterpret_cast<e_t*>(sub[0]);
   t->args = reinterpret_cast<e_t*>(sub[2]);
   t->line = sub[0]->line;
   t->type2 = tk::call;
   s.push_back(t);
}


fn tree_of_dots_to_id_path(e_t* t, vec<id_t*>& id_) -> void
{
   while (true) {
      if (t->type2 == tk::id) {
         id_.push_back(reinterpret_cast<id_t*>(t));
         return;
      }
      else if (t->type2 == tk::dot) {
         dot_t* op = reinterpret_cast<dot_t*>(t);
         if (op->l->type2 != tk::id) break;
         id_.push_back(reinterpret_cast<id_t*>(op->l));
         t = op->r;
      }
      else break;
   }
   throw "ERROR! There is must to be list of id-s separated with dots, on the line " + std::to_string(t->line);
}

fn shorten_the_path(vec<id_t*>& id_s) -> void
{
   for (size_t idx = 0; id_s.size() > idx; ++idx) {
      if (id_s[idx]->p->str == "_") {
         id_s.erase(std::begin(id_s) + idx);
         if (idx != 0) {
            id_s.erase(std::begin(id_s) + idx - 1);
         }
      }
   }
}


struct fun_t : public e_t { vec<id_t*> id_; e_t *args; e_t *out; e_t *b; void let_in(VISITOR*v)override{v->visit(this);} };
fn cb_fun(vec<tok*> &sub, vec<tok*> &s) -> void
{
   fun_t* f = add_tok(new fun_t());
   e_t* e_id = reinterpret_cast<e_t*>(sub[1]);
   tree_of_dots_to_id_path(e_id, f->id_);
   shorten_the_path(f->id_);
   f->args  = reinterpret_cast<e_t*>(sub[3]);
   f->out   = reinterpret_cast<e_t*>(sub[5]);
   f->b     = reinterpret_cast<e_t*>(sub[7]);
   f->line  = sub[0]->line;
   f->type2 = tk::fun;
   s.push_back(f);
}
fn cb_fun_rn_emp(vec<tok*> &sub, vec<tok*> &s) -> void
{
   fun_t* f = add_tok(new fun_t());
   e_t* e_id = reinterpret_cast<e_t*>(sub[1]);
   tree_of_dots_to_id_path(e_id, f->id_);
   shorten_the_path(f->id_);
   f->args  = reinterpret_cast<e_t*>(sub[3]);
   f->out   = nullptr;
   f->b     = reinterpret_cast<e_t*>(sub[6]);
   f->line  = sub[0]->line;
   f->type2 = tk::fun;
   s.push_back(f);
}

struct rn_t : public e_t { e_t *res; void let_in(VISITOR*v)override {v->visit(this);} };
fn cb_rn(vec<tok*> &sub, vec<tok*> &s) -> void
{
   rn_t* r = add_tok(new rn_t());
   r->type = tk::e;
   r->type2 = tk::rn;
   r->res = reinterpret_cast<e_t*>(sub[1]);
   r->line = sub[0]->line;
   s.push_back(r);
}


struct ns_t : public e_t { e_t *id, *b; void let_in(VISITOR*v)override {v->visit(this);} };
fn cb_ns(vec<tok*> &sub, vec<tok*> &s) -> void
{
   ns_t* n = add_tok(new ns_t());
   n->type2 = tk::ns;
   n->id = reinterpret_cast<e_t*>(sub[1]);
   n->b = reinterpret_cast<e_t*>(sub[3]);
   CURR_PTH.pop_back();
}


struct mtch_t : public e_t { id_t* id_; std::map<e_t*, e_t*> v; void let_in(VISITOR*v)override { v->visit(this); } };
fn cb_mtch(vec<tok*> &sub, vec<tok*> &s) -> void
{
   mtch_t* m = add_tok(new mtch_t());
   m->type2 = tk::mtch;

   e_t* e_id = reinterpret_cast<e_t*>(sub[1]);
   if (e_id->type2 != tk::id) throw "ERROR! There is must to be an id in match rule, in line " + std::to_string(e_id->line);
   m->id_ = reinterpret_cast<id_t*>(sub[1]);

   e_t* e_body_or_assign = reinterpret_cast<e_t*>(sub[4]);

   if (e_body_or_assign->type2 == tk::body) {
      body_t* b = reinterpret_cast<body_t*>(e_body_or_assign);
      for (auto& t : b->ts) {
         if (t->type2 != tk::asg) throw "ERROR! There is must to be an assignment in match rule, in line " + std::to_string(t->line);
         asg_t* a = reinterpret_cast<asg_t*>(t);
         m->v[a->l] = a->r;
      }
   }
   else if (e_body_or_assign->type2 == tk::asg) {
      asg_t* a = reinterpret_cast<asg_t*>(e_body_or_assign);
      m->v[a->l] = a->r;
   }
   else throw std::string("ERROR! There is must to be only assignment operators in body of match rule, in line ") + std::to_string(e_body_or_assign->line);
   s.push_back(m);
}


struct induc_t : public e_t { id_t *id_; vec<id*> cnstr_id; vec<call_t*> cnstr_rec; };
fn cb_induc(vec<tok*> &sub, vec<tok*> &s) -> void
{
   induc_t* i = add_tok(new induc_t());
   i->type2 = tk::induc;

   e_t* id_ = reinterpret_cast<e_t*>(sub[1]);
   if (id_->type2 != tk::id) throw "ERROR! There is must to be an id of your type, on the line " + std::to_string(id_->line);
   i->id_ = reinterpret_cast<id_t*>(id_);

   e_t* l = reinterpret_cast<e_t*>(sub[4]);
   if (l->type2 == tk::body) {
      body_t* b = reinterpret_cast<body_t*>(l);
      for (auto& t : b->ts) {
         if (t->type2 != tk::id) {
            i->cnstr_id.push_back(reinterpret_cast<id_t*>(t)->p);
            continue;
         }
         if (t->type2 != tk::call) {
            i->cnstr_rec.push_back(reinterpret_cast<call_t*>(t));
            continue;
         }
         throw "ERROR! Constuctor of inductive types may be only an id or record call. In line " + std::to_string(t->line);
      }
   } else if (l->type2 != tk::id) {
      i->cnstr_id.push_back(reinterpret_cast<id_t*>(l)->p);
   } else if (l->type2 != tk::call) {
      i->cnstr_rec.push_back(reinterpret_cast<call_t*>(l));
   } else {
      throw "ERROR! Constuctor of inductive types may be only an id or record call. In line " + std::to_string(l->line);
   }
   std::string str_;
   for (auto& dir_ : CURR_PTH) str_ += dir_->p->str + '.';
   str_ += i->id_->p->str;
   if (TYPES_POOL.count(str_)) throw "ERROR! Type '" + str_ + "' defined multiple times. The first one is on the line " + std::to_string(TYPES_POOL[str_]->line) + " and the second one is on the line " + std::to_string(i->line);
   TYPES_POOL[str_] = i;
}

struct struc_t : public e_t { id_t *id_; struc_t* parent{ nullptr }; vec<std::pair<id_t*, e_t*>> fields; };
std::map<struc_t*, struc_t*> inheritance_tbl;
fn cb_struc(vec<tok*> &sub, vec<tok*> &s) -> void
{
   struc_t *st = add_tok(new struc_t());
   st->type2 = tk::struc;

   e_t* id_ = reinterpret_cast<e_t*>(sub[1]);
   if (id_->type2 != tk::id && id_->type2 != tk::is) 
   
   if (id_->type2 == tk::id) {
      st->id_ = reinterpret_cast<id_t*>(id_);
   }
   else if (id_->type2 == tk::is) {

      is_t* is_ = reinterpret_cast<is_t*>(id_);
      if (is_->l->type2 != tk::id) throw "ERROR! There is must to be id of your type, on the line " + std::to_string(is_->l->line);
      st->id_ = reinterpret_cast<id_t*>(is_->l);
      std::string str_;
      if (is_->r->type2 == tk::dot) {
         std::vector<id_t*> pth_ = CURR_PTH;
         tree_of_dots_to_id_path(is_->r, pth_);
         shorten_the_path(pth_);
         bool first = true;
         for (auto& id_ : pth_) { if (!first) str_ += '.'; else first = false; str_ += id_->p->str; }
      }
      else if (is_->r->type2 == tk::id) {
         str_ = reinterpret_cast<id_t*>(is_->r)->p->str;
      }
      else throw "ERROR! There is must to be id of previously defined struct type, on the line " + std::to_string(is_->r->line);

      if (!TYPES_POOL.count(str_)) throw "ERROR! Struct can inherent only types that already defined, on the line " + std::to_string(is_->r->line);
      if (TYPES_POOL[str_]->type2 != tk::struc) throw "ERROR! Parent of struct must to be a struct, on the line " + std::to_string(is_->r->line);
      st->parent = reinterpret_cast<struc_t*>(TYPES_POOL[str_]);
   }
   else throw "ERROR! There is must to be an id of your type, on the line " + std::to_string(id_->line);

   e_t* t = reinterpret_cast<e_t*>(sub[3]);
   if (t->type2 == tk::body) {
      body_t* b = reinterpret_cast<body_t*>(t);
      for (auto& s_ : b->ts) {
         if (s_->type2 != tk::is) throw "ERROR! Struct may contaion only announcements of fields, on the line " + std::to_string(s_->line);
         is_t* is_ = reinterpret_cast<is_t*>(s_);
         if (is_->l->type2 != tk::id) throw "ERROR! Struct may contaion only announcements of fields, on the line " + std::to_string(is_->l->line);
         st->fields.push_back({ reinterpret_cast<id_t*>(is_->l), is_->r });
      }
   }

   std::string str_;
   bool first = true;
   for (auto& dir_ : CURR_PTH) str_ += dir_->p->str + '.';
   str_ += st->id_->p->str;
   if (TYPES_POOL.count(str_)) throw "ERROR! Type '" + str_ + "' defined multiple times. The first one is on the line " + std::to_string(TYPES_POOL[str_]->line) + " and the second one is on the line " + std::to_string(st->line);
   TYPES_POOL[str_] = st;
}

vec<RULE> rules{
   {{tk::ns, tk::e, tk::He, tk::eol, tk::e, tk::eol, tk::sem} , 14, cb_ns}
,  {{tk::induc, tk::id, tk::asg, tk::eol, tk::e, tk::eol, tk::dot}, 13, cb_induc}
,  {{tk::induc, tk::e , tk::asg, tk::eol, tk::e, tk::eol, tk::dot}, 13, cb_induc}
,  {{tk::induc, tk::id, tk::eol, tk::e, tk::eol, tk::dot}, 13, cb_struc}
,  {{tk::induc, tk::e , tk::eol, tk::e, tk::eol, tk::dot}, 13, cb_struc}

,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::e,      tk::eol, tk::e, tk::eol, tk::dot}, 13, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::id,     tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::i32_tp, tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::i64_tp, tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::i128_tp,tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::f32_tp, tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::f64_tp, tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::str_tp, tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun}
,  {{tk::fun, tk::e, tk::opn, tk::e, tk::cls, tk::eol, tk::e, tk::eol, tk::dot}, 12, cb_fun_rn_emp}

,  {{tk::mtch, tk::id, tk::in, tk::eol, tk::e, tk::eol, tk::dot} , 12, cb_mtch}
,  {{tk::mtch, tk::e,  tk::in, tk::eol, tk::e, tk::eol, tk::dot} , 12, cb_mtch}

,  {{tk::e, tk::dot, tk::e}, 11, cb_bOp<dot_t, tk::dot>}
,  {{tk::e, tk::opn, tk::e, tk::cls}, 10, cb_call}
,  {{tk::e, tk::npu, tk::e, tk::uHa, tk::e}, 9, cb_cond}
,  {{tk::opn, tk::e, tk::forch, tk::e, tk::com, tk::e, tk::col, tk::e, tk::npu, tk::e, tk::cls}, 9, cb_forch<3,5,7,1,9>}
,  {{tk::opn, tk::e, tk::forch, tk::e, tk::col, tk::e, tk::npu, tk::e, tk::cls}                , 9, cb_forch<0,3,5,1,7>}
,  {{tk::opn, tk::e, tk::forch, tk::e, tk::com, tk::e, tk::col, tk::e, tk::cls}                , 9, cb_forch<3,5,7,1,0>}
,  {{tk::opn, tk::e, tk::forch, tk::e, tk::col, tk::e, tk::cls}                                , 9, cb_forch<0,3,5,1,0>}
,  {{tk::fun, tk::opn, tk::e, tk::cls,tk::dot,tk::e}, 9, cb_lam}
,  {{tk::opn, tk::e, tk::cls}, 8, cb_par}
,  {{tk::opn, tk::cls}, 8, cb_empty_par}

,  {{tk::uHa, tk::e,   tk::com, tk::e, tk::eol, tk::uHa_}, 9, cb_npu<1,3,5>}
,  {{tk::uHa, tk::e,   tk::com, tk::e, tk::eol, tk::dot} , 9, cb_npu<1,3,0>}
,  {{tk::uHa, tk::com, tk::e,   tk::eol, tk::dot}        , 9, cb_npu<0,2,0>}

,  {{tk::npu, tk::e, tk::com, tk::e, tk::eol, tk::dot }, 8, cb_npu<1,3,0>}
,  {{tk::npu, tk::e, tk::com, tk::e, tk::eol, tk::uHa_}, 8, cb_npu<1,3,5>}
,  {{tk::whl, tk::e, tk::com, tk::e, tk::eol, tk::dot }, 8, cb_whl}

,  {{tk::forch, tk::opn, tk::e, tk::com, tk::e, tk::col, tk::e, tk::cls, tk::com, tk::e, tk::eol, tk::dot}, 8, cb_for<2,4,6,9>}
,  {{tk::forch, tk::opn, tk::e, tk::col, tk::e, tk::cls, tk::com, tk::e, tk::eol, tk::dot}                , 8, cb_for<0,2,4,7>}
,  {{tk::forch, tk::e, tk::com, tk::e, tk::col, tk::e, tk::com, tk::e, tk::eol, tk::dot}                  , 8, cb_for<1,3,5,7>}
,  {{tk::forch, tk::e, tk::col, tk::e, tk::com, tk::e, tk::eol, tk::dot}                                  , 8, cb_for<0,1,3,5>}

,  {{tk::sub, tk::e}, 7, cb_uOp<u_sub_t, tk::u_sub>}, {{tk::add, tk::e}, 7, cb_u_add}, {{tk::He, tk::e}, 7, cb_uOp<He_t,tk::He>}

,  {{tk::e, tk::mul, tk::e}, 6, cb_bOp<mul_t,tk::mul>}, {{tk::e, tk::sep, tk::e}, 6, cb_bOp<sep_t,tk::sep>}
,  {{tk::e, tk::add, tk::e}, 5, cb_bOp<add_t,tk::add>}, {{tk::e, tk::sub, tk::e}, 5, cb_bOp<sub_t,tk::sub>}

,  {{tk::e, tk::mor_eq, tk::e}, 4, cb_bOp<mor_eq_t,tk::mor_eq>}, {{tk::e, tk::les_eq, tk::e}, 4, cb_bOp<les_eq_t,tk::les_eq>}
,  {{tk::e, tk::mor   , tk::e}, 4, cb_bOp<   mor_t,tk::mor   >}, {{tk::e, tk::les   , tk::e}, 4, cb_bOp<   les_t,tk::les   >}
,  {{tk::e, tk::eq    , tk::e}, 4, cb_bOp<    eq_t,tk::eq    >}, {{tk::e, tk::He_eq , tk::e}, 4, cb_bOp< He_eq_t,tk::He_eq >}
,  {{tk::e, tk::col   , tk::e}, 4, cb_bOp<    is_t,tk::is    >}

,  {{tk::e, tk::com, tk::e}, 3, cb_seq<seq_t, tk::seq>}
,  {{tk::i32}, 2, cb_one<i32,i32_t>}, {{tk::i64}, 2, cb_one<i64,i64_t>}, {{tk::i128}, 2, cb_one<i128,i128_t>}
,  {{tk::f32}, 2, cb_one<f32,f32_t>}, {{tk::f64}, 2, cb_one<f64,f64_t>}, {{tk::id  }, 2, cb_one<  id,id_t  >}, {{tk::str  }, 2, cb_one<  str,str_t  >}
,  {{tk::i32_tp}, 2, cb_one_tp<i32_tp_t>}, {{tk::i64_tp}, 2, cb_one_tp<i64_tp_t>}, {{tk::i128_tp}, 2, cb_one_tp<i128_tp_t>}
,  {{tk::f32_tp}, 2, cb_one_tp<f32_tp_t>}, {{tk::f64_tp}, 2, cb_one_tp<f64_tp_t>}, {{tk::str_tp }, 2, cb_one_tp< str_tp_t>}
,  {{tk::rn, tk::e}, 1, cb_rn}
,  {{tk::cnt}, 1, cb_atom<cnt_t>}, {{tk::br}, 1, cb_atom<br_t>}
,  {{tk::e, tk::asg, tk::e}, 1, cb_bOp< asg_t,tk::asg >}
,  {{tk::e, tk::eol, tk::e}, 0, cb_seq<body_t,tk::body>}
};


fn eq_lists(vec<tok*>& l1, vec<tk>& l2) -> bool
{
   if (l1.size() != l2.size()) return false;
   for (size_t i = 0; l1.size() > i; ++i) {
      if (l1[i]->type != l2[i]) return false;
   }
   return true;
}


fn is_in(vec<tok*>& l, vec<RULE>& r) -> RULE
{
   for (auto& i : r) {
      if (eq_lists(l, i.s)) return i;
   }
   return { {}, -1, {} };
}


fn starts_with(vec<tk>& lst, vec<tok*>& bgn) -> bool
{
   if (bgn.size() > lst.size()) return false;
   for (size_t i = 0; bgn.size() > i; ++i) {
      if (bgn[i] == nullptr || lst[i] != bgn[i]->type) return false;
   }
   return true;
}


fn is_in_start(vec<tok*>& l, vec<RULE>& r) -> int
{
   for (auto& i : r) {
      if (starts_with(i.s, l)) return i.power;
   }
   return -1;
}


fn print(tk type) -> void
{
   using namespace std;
   if (type == tk::e) { cout.put('e');   return;}
   if (type == tk::add) { cout.put('+'); return;}
   if (type == tk::mul) { cout.put('*'); return;}
   if (type == tk::eq) { cout.put('=');  return;}
   if (type == tk::com) { cout.put(','); return;}
   if (type == tk::sem) { cout.put(';'); return;}
   if ( type == tk::col
   ||   type == tk::is
   ||   type == tk::in ){ cout.put(':'); return;}
   if (type == tk::npu) { cout.put('?'); return;}
   if (type == tk::asg) { cout.put('='); return;}
   if (type == tk::uHa) { cout << "els";return;}
   if (type == tk::uHa_) { cout << "els_b";return;}
   if (type == tk::uHanpu) { cout << "elif"; return; }
   if (type == tk::fun) { cout.put('\\');return;}

   if (type == tk::forch) { cout << "for_";return;}
   if (type == tk::for_x) { cout << "for_x";return;}
   if (type == tk::for_i) { cout << "for_i_x";return;}
   if (type == tk::forch_i) { cout << "forch_i_x";return;}
   if (type == tk::forch_x) { cout << "forch_x";return;}
   if (type == tk::forch_i_c) { cout << "forch_i_x_c";return;}
   if (type == tk::forch_x_c) { cout << "forch_x_c";return;}

   if (type == tk::i32) { cout.put('2'); return;}
   if (type == tk::opn) { cout.put('('); return;}
   if (type == tk::cls) { cout.put(')'); return;}
   if (type == tk::eol) { cout.put('\n');return;}
   if (type == tk::dot) { cout.put('.'); return;}
   if (type == tk::str) { cout.put('"'); return;}
   if (type == tk::id) { cout.put('i');  return;}
   if (type == tk::call) { cout << "call";  return;}
   if (type == tk::body) { cout << "body";  return;}
   if (type == tk::ns) { cout << "ns";  return;}
   if (type == tk::rn) { cout << "rn";  return;}
   if (type == tk::mtch) { cout << "mt";  return;}
   if (type == tk::induc) { cout << "T";  return;}
}


fn print(tok* t) -> void
{
   using namespace std;
   if (t->type == tk::e) {
      cout.put('[');
      print(reinterpret_cast<e_t*>(t)->type2);
      cout.put(']');
   }
   else {
      print(t->type);
   }
}


fn print(vec<tok*>& ts) ->void
{
   using namespace std;
   for (auto& t : ts) {
      print(t);
   }
   cout << endl;
}

fn check_for_new_ns(vec<tok*>& stack) -> void
{
   size_t sz = stack.size();
   if (sz > 1) {
      tok *c = stack[sz - 2], *a = stack.back();
      if (c->type == tk::ns) {
         if (a->type == tk::e && reinterpret_cast<e_t*>(a)->type2 == tk::id) {
            CURR_PTH.push_back(reinterpret_cast<id_t*>(a));
         }
      }
   }
}

fn parse(vec<tok*>& s) -> body_t*
{
   init_std_types();

   vec<tok*> stack = {};

   for (size_t j = 1; s.size() >= j; ++j) {
      tok *c = s[j - 1]
      ,   *a = s.size() > j ? s[j] : nullptr
      ;

      stack.push_back(c);
      for (size_t i = 0; stack.size() > i;) {
         vec<tok*> sub_stack{ std::begin(stack) + i, std::end(stack) };
         RULE r = is_in(sub_stack, rules);

         if (!r.s.empty()) {
            vec<tok*> stack2 = stack;
            stack2.push_back(a);
            int p2 = r.power;

            for (size_t k = 0; stack2.size() - 1 > k; ++k) {
               vec<tok*> sub_stack2{ std::begin(stack2) + k, std::end(stack2) };
               p2 = is_in_start(sub_stack2, rules);
               if (p2 != -1) break;
            }

            if (r.s.size() > 1 && p2 > r.power) break;
            
            std::puts("\n\n");
            print(stack);
            stack = vec<tok*>(std::begin(stack), std::begin(stack) + i);
               std::puts("---");
               print(stack);
               std::puts("+++");
               print(sub_stack);

            r.cb(sub_stack, stack);
            std::puts("===");
            i = 0;
            print(stack);
            check_for_new_ns(stack);
            std::cin.get();
            continue;
         }
         ++i;
      }
   }
   return stack.empty() ? nullptr : reinterpret_cast<body_t*>(reinterpret_cast<e_t*>(stack[0]));
}


#endif // PARSR_HPP
