#ifndef LEXR_H
#define LEXR_H

#include <fstream>
#include <iostream>
#include <string>
#include <iterator>
#include <streambuf>
#include <memory>
#include <map>
#include <set>
#include <vector>
#include <utility>

#include "visitor_base.hpp"

#define fn auto
template <typename...T>using tup = std::tuple<T...>;
template <typename T>  using vec = std::vector<T>;

enum class tk {
     id
   , i32, i64, i128, f32, f64
   , i32_tp, i64_tp, i128_tp, f32_tp, f64_tp, str_tp
   , str
   , rn, br, cnt
   , lam, add, sub, mul, sep, dot, com, col, fun, sem
   , opn, cls, opb, clb, opc, clc, cls_body
   , mor, les, eq, mor_eq, les_eq, He_eq
   , asg
   , u, unu, He, u_sub
   , dog, hsh, dlr, qst
   , eol

   , whl, forch, npu, uHa, uHa_, uHanpu, ite
   , ns, induc, struc
   , mtch, in
   , e
   , for_x, for_i, forch_x, forch_i
   , forch_x_c, forch_i_c, cond, call
   , seq, body, is
   , eof
};


struct tok { tk type; size_t line; virtual void let_in(VISITOR*v) {}; };


extern std::vector<std::unique_ptr<tok>> tok_pool;
extern std::vector<tok*>* pt;
extern size_t line;
extern std::map<std::string, tk> base_types_id_s;

template<typename T> fn add_tok(T* new_tok)->T*
{
   tok_pool.push_back(std::unique_ptr<tok>(new_tok));
   new_tok->line = line;
   if (pt != nullptr) pt->push_back(tok_pool.back().get());

   return new_tok;
}


struct id   : public tok { std::string str; };
struct str  : public tok { std::string   s; };
struct i32  : public tok { int           n; };
struct i64  : public tok { long          n; };
struct i128 : public tok { long long     n; };
struct f32  : public tok { float         n; };
struct f64  : public tok { double        n; };


fn read_tokens(const char* input_path, std::vector<tok*>& output) -> void;

fn test_with(std::vector<tok*>& input, const char text_path[]) -> bool;

#endif
