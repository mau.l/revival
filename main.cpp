#include "lexr.hpp"
#include "parsr.hpp"

#include "semantic_analyzer.hpp"
#include <locale>




int main ()
{
   std::vector<tok*> tokens;
   read_tokens("main.txt", tokens);
   print(tokens);
   body_t* ast = parse(tokens);

   SEMANTIC_ANALYZER sema;
   ast->let_in(&sema);
   //sema.visit(ast);

   //if (!test_with(tokens, "test.txt")) std::cout << "ERROR!!!!";

   std::cin.get();
   return 0;
}
