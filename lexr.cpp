#include "lexr.hpp"

extern std::vector<std::unique_ptr<tok>> tok_pool;
extern std::map<std::string, tk> base_types_id_s{
 { u8"�32"   , tk::i32_tp} 
,{ u8"�64"   , tk::i64_tp}
,{ u8"�128"  , tk::i128_tp}
,{ u8"�32"   , tk::f32_tp}
,{ u8"�64"   , tk::f64_tp}
,{ u8"������", tk::str_tp}
};

static std::map<std::string, std::string> cyr_utf8_to_ascii{
   {u8"�",  "a"},  {u8"�",  "A"}
,  {u8"�",  "o"},  {u8"�",  "O"}
,  {u8"�",  "c"},  {u8"�",  "C"}
,                  {u8"�",  "M"}
,                  {u8"�",  "K"}
,                  {u8"�",  "B"}
,  {u8"�",  "p"},  {u8"�",  "P"}
,  {u8"�",  "e"},  {u8"�",  "E"}
,                  {u8"�",  "T"}
,                  {u8"�",  "H"}
,  {u8"�",  "x"},  {u8"�",  "X"}

,  {u8"�", "zd"}, {u8"�", "zD"} ,  {u8"�", "Zs"}, {u8"�", "ZS"}
,  {u8"�", "zb"}                ,  {u8"�", "Zd"}, {u8"�", "ZD"}
,  {u8"�", "zf"}, {u8"�", "zF"} ,  {u8"�", "Zf"}, {u8"�", "ZF"}
,  {u8"�", "zg"}, {u8"�", "zG"} ,  {u8"�", "Zg"}, {u8"�", "ZG"}
,  {u8"�", "zi"}, {u8"�", "zI"} ,  {u8"�", "Zi"}, {u8"�", "ZI"}
,  {u8"�", "zj"}, {u8"�", "zJ"} ,  {u8"�", "Zj"}, {u8"�", "ZJ"}
,  {u8"�", "zh"}, {u8"�", "zk"} ,  {u8"�", "Zl"}, {u8"�", "ZL"}
,  {u8"�", "zl"}, {u8"�", "zL"} ,  {u8"�", "Zn"}, {u8"�", "ZN"}
,  {u8"�", "zn"}, {u8"�", "zN"} ,  {u8"�", "Zq"}, {u8"�", "ZQ"}
,  {u8"�", "zm"}
,  {u8"�", "zq"}, {u8"�", "zQ"}
,  {u8"�", "zr"}
,  {u8"�", "zR"}
,  {u8"�", "zs"}, {u8"�", "zS"}
,  {u8"�", "zt"}
,  {u8"�", "zu"}, {u8"�", "zU"}
,  {u8"�", "zv"}, {u8"�", "zV"}
,  {u8"�", "zw"}, {u8"�", "zW"}
,  {u8"z", "zz"}, {u8"Z", "ZZ"}
};


static std::map<std::string, std::string> ascii_to_cyr_utf8{
   {"a",  u8"�"}, {"A" , u8"�"}
,  {"o",  u8"�"}, {"O" , u8"�"}
,  {"c",  u8"�"}, {"C" , u8"�"}
,                 {"M" , u8"�"}
,                 {"K" , u8"�"}
,                 {"B" , u8"�"}
,  {"p",  u8"�"}, {"P" , u8"�"}
,  {"e",  u8"�"}, {"E" , u8"�"}
,                 {"T" , u8"�"}
,                 {"H" , u8"�"}
,  {"x",  u8"�"}, {"X" , u8"�"}

,  {"zd", u8"�"}, {"zD", u8"�"} ,  {"Zs", u8"�"}, {"ZS", u8"�"}
,  {"zb", u8"�"}                ,  {"Zd", u8"�"}, {"ZD", u8"�"}
,  {"zf", u8"�"}, {"zF", u8"�"} ,  {"Zf", u8"�"}, {"ZF", u8"�"}
,  {"zg", u8"�"}, {"zG", u8"�"} ,  {"Zg", u8"�"}, {"ZG", u8"�"}
,  {"zi", u8"�"}, {"zI", u8"�"} ,  {"Zi", u8"�"}, {"ZI", u8"�"}
,  {"zj", u8"�"}, {"zJ", u8"�"} ,  {"Zj", u8"�"}, {"ZJ", u8"�"}
,  {"zh", u8"�"}, {"zk", u8"�"} ,  {"Zl", u8"�"}, {"ZL", u8"�"}
,  {"zl", u8"�"}, {"zL", u8"�"} ,  {"Zn", u8"�"}, {"ZN", u8"�"}
,  {"zn", u8"�"}, {"zN", u8"�"} ,  {"Zq", u8"�"}, {"ZQ", u8"�"}
,  {"zm", u8"�"}
,  {"zq", u8"�"}, {"zQ", u8"�"}
,  {"zr", u8"�"}
,  {"zR", u8"�"}
,  {"zs", u8"�"}, {"zS", u8"�"}
,  {"zt", u8"�"}
,  {"zu", u8"�"}, {"zU", u8"�"}
,  {"zv", u8"�"}, {"zV", u8"�"}
,  {"zw", u8"�"}, {"zW", u8"�"}
,  {"zz", u8"z"}, {"ZZ", u8"Z"}
};

std::vector<std::unique_ptr<tok>> tok_pool;
std::vector<tok*>* pt{ nullptr };
size_t line{ 0 };


inline
fn is_last_tok(tk type) -> bool
{
   return pt != nullptr && !pt->empty() && pt->back()->type == type;
}


inline
fn reset_last_tok_type(tk type) -> void
{
   pt->back()->type = type;
}


fn cyr_utf8_ids_to_ascii(std::string& source, std::string& output) -> void
{
   std::string buf;
   bool is_comment = false;
   bool is_str = false;

   for (auto a : source) {
      if (is_comment) { if (a == '\n') is_comment = false; else continue; }

      if (buf.empty() && a >= -1 && a <= 255) {

         if (!is_comment && !is_str && a == '/' && !output.empty() && output.back() == '/') {
            is_comment = true;
            output.pop_back();
            continue;
         }

         if (a != 'z' && a != 'Z') {
            if (a == '"' || a == '\'' && output.back() != '\\') is_str = !is_str;
            output.push_back(a);
            continue;
         }

      }

      if (is_str) {
         output.push_back(a);
      }
      else {
         buf.push_back(a);
         if (cyr_utf8_to_ascii.count(buf)) {
            output.append(cyr_utf8_to_ascii[buf]);

            buf.clear();
         }
      }
   }
}


fn hex(char c) -> char const*
{
   const char REF[] = "0123456789ABCDEF";
   static char output[3] = "XX";
   output[0] = REF[0x0f & c >> 4];
   output[1] = REF[0x0f & c];
   return output;
}


fn add_sym_token(char a) -> void
{
   if (a != ' ' && a != '\t') {
      if (a == '(') add_tok(new tok)->type = tk::opn;
      if (a == ')') add_tok(new tok)->type = tk::cls;
      if (a == '[') add_tok(new tok)->type = tk::opb;
      if (a == ']') add_tok(new tok)->type = tk::clb;
      if (a == '{') add_tok(new tok)->type = tk::opc;
      if (a == '}') add_tok(new tok)->type = tk::clc;

      if (a == '<') add_tok(new tok)->type = tk::les;
      if (a == '>') add_tok(new tok)->type = tk::mor;
      if (a == '=') {
         if (is_last_tok(tk::asg)) reset_last_tok_type(tk::eq);
         else if (is_last_tok(tk::les)) reset_last_tok_type(tk::les_eq);
         else if (is_last_tok(tk::mor)) reset_last_tok_type(tk::mor_eq);
         else if (is_last_tok(tk::He)) reset_last_tok_type(tk::He_eq);
         else add_tok(new tok)->type = tk::asg;
      }
      if (a == '!') add_tok(new tok)->type = tk::He;
      if (a == '|') add_tok(new tok)->type = tk::unu;
      if (a == '&') add_tok(new tok)->type = tk::u;

      if (a == '.') {
         // while (pt != nullptr && is_last_tok(tk::eol)) pt->pop_back();
         add_tok(new tok)->type = tk::dot;
      }
      if (a == ',') {
         while (pt != nullptr && is_last_tok(tk::eol)) pt->pop_back();
         add_tok(new tok)->type = tk::com;
      }
      if (a == ':') add_tok(new tok)->type = tk::col;
      if (a == '@') add_tok(new tok)->type = tk::dog;
      if (a == '#') add_tok(new tok)->type = tk::hsh;
      if (a == '$') add_tok(new tok)->type = tk::dlr;
      if (a == '?') add_tok(new tok)->type = tk::qst;
      if (a == '\\')add_tok(new tok)->type = tk::fun;
      if (a == ';') add_tok(new tok)->type = tk::sem;

      if (a == '+') add_tok(new tok)->type = tk::add;
      if (a == '-') add_tok(new tok)->type = tk::sub;
      if (a == '/') add_tok(new tok)->type = tk::sep;
      if (a == '*') add_tok(new tok)->type = tk::mul;

      if (a == '\n' && !is_last_tok(tk::eol) && !is_last_tok(tk::com)) {
         if (is_last_tok(tk::dot) && pt->size() > 2) {
            auto beg = pt->begin() + (pt->size() - 2);
            if (tk::eol != (*beg)->type) {
               auto pt2 = pt;
               tok* p;
               pt = nullptr;
               add_tok(p = new tok)->type = tk::eol;
               pt = pt2;
               pt->insert(beg+1, p);
            }
         }
         add_tok(new tok)->type = tk::eol;
      }
   }
}


fn is_He_exception(tk type) -> bool
{
   bool res = false;

   if (is_last_tok(tk::He)) {
      if (type == tk::mor) {
         reset_last_tok_type(tk::les_eq);
         res = true;
      }
      else if (type == tk::les) {
         reset_last_tok_type(tk::mor_eq);
         res = true;
      }
   }

   return res;
}

fn translate_std_type_ids() -> void
{
   std::map<std::string, tk> id_s2;
   std::swap(base_types_id_s, id_s2);
   for (auto& id_ : id_s2) {
      std::string from{ id_.first }, to;
      cyr_utf8_ids_to_ascii(from, to);
      base_types_id_s[to] = id_.second;
   }
}

fn tokenize(std::string& source) -> void
{
   bool is_dstr = false, is_str = false, ordered_add = false;
   enum state_t { num, alpha, sym } prew_state{ state_t::sym };

   std::string symbols{ ",[](){}+-*/=&%$#@!|;:\\" }, buff;

   std::map<std::string, tk> keys{
      { u8"����"  , tk::whl    }
   ,  { u8"���"   , tk::npu    }
   ,  { u8"�����" , tk::uHa    }
   ,  { u8"������", tk::uHanpu }
   ,  { u8"���"   , tk::forch  }
   ,  { u8"��"    , tk::He     }
   ,  { u8"�"     , tk::u      }
   ,  { u8"���"   , tk::unu    }
   ,  { u8"������", tk::mor    }
   ,  { u8"������", tk::les    }

   ,  { u8"�����" , tk::rn     }
   ,  { u8"����"  , tk::br     }
   ,  { u8"�����" , tk::cnt    }

   ,  { u8"����"  , tk::induc  }
   ,  { u8"�����" , tk::struc  }
   ,  { u8"������", tk::mtch   }
   ,  { u8"��"    , tk::in     }
   ,  { u8"�������",tk::ns    }
   };
   translate_std_type_ids();

   {
      std::map<std::string, tk> keys2;
      std::swap(keys, keys2);
      for (auto& key : keys2) {
         std::string from{ key.first }, to;
         cyr_utf8_ids_to_ascii(from, to);
         keys[to] = key.second;
      }
      for (auto& id_ : base_types_id_s) {
         keys[id_.first] = id_.second;
      }
   }

   for (auto a : source) {

      if (is_str && a != '\'') {
         buff.push_back(a);
         continue;
      }

      if (is_dstr && a != '"') {
         buff.push_back(a);
         continue;
      }

      // -- when string is ended --
      if (is_str || is_dstr) {
         str* pstr;
         prew_state = state_t::sym;
         is_str = is_dstr = false;
         add_tok(pstr = new str)->type = tk::str;
         pstr->s = buff;
         buff.clear();
      }
      else {
         is_dstr = (a == '"');
         is_str = (a == '\'');

         // -- when string begins --
         if (is_dstr || is_str) {
            ordered_add = !buff.empty();
            prew_state = state_t::sym;
         }
      }

      if (a == '_' || isalpha(a)) {
         ordered_add = !buff.empty() && (prew_state == state_t::num);
         if (!ordered_add) buff.push_back(a);
         prew_state = state_t::alpha;
      }

      if (isdigit(a) || (a == '.' && prew_state == state_t::num)) {
         ordered_add = !buff.empty() && (prew_state == state_t::sym);
         if (!ordered_add) buff.push_back(a);
         prew_state = state_t::num;
      }

      if (a == '\n') {
         ++line;
         ordered_add = !buff.empty();
         if (!ordered_add && is_last_tok(tk::dot)) {
            //reset_last_tok_type (tk::cls_body);
            if (!ordered_add) add_sym_token('\n');
         }
         else if (prew_state == state_t::sym && !ordered_add && !is_last_tok(tk::eol)) {
            add_sym_token('\n');
         }
         prew_state = state_t::sym;
      }

      if (a == '\t' || a == ' ') {
         ordered_add = !buff.empty();
         prew_state = state_t::sym;

      }
      else if ((prew_state != state_t::num && a == '.') || symbols.find(a) != std::string::npos) {

         prew_state = state_t::sym;
         ordered_add = !buff.empty();
         if (!ordered_add) add_sym_token(a);

      }

      if (ordered_add) {

         if (isdigit(buff[0])) {

            if (buff.find('.') != std::string::npos) {

               if (a == 'f') {
                  f32* pf;
                  add_tok(pf = new f32)->type = tk::f32;
                  pf->n = atof(buff.c_str());
                  prew_state = state_t::sym;
               }
               else {
                  f64* pf;
                  add_tok(pf = new f64)->type = tk::f64;
                  pf->n = atof(buff.c_str());
               }

            }
            else {

               if (a == 'L') {
                  i128* pn;
                  add_tok(pn = new i128)->type = tk::i128;
                  pn->n = atoll(buff.c_str());
                  prew_state = state_t::sym;
               }
               else if (a == 'l') {
                  i64* pn;
                  add_tok(pn = new i64)->type = tk::i64;
                  pn->n = atol(buff.c_str());
                  prew_state = state_t::sym;
               }
               else {
                  i32* pn;
                  add_tok(pn = new i32)->type = tk::i32;
                  pn->n = atoi(buff.c_str());
               }

            }
            buff.clear();

         }
         else {

            if (keys.count(buff)) {
               tk type = keys[buff];

               if (!is_He_exception(type)) {
                  add_tok(new tok)->type = type;
               }
            }
            else {
               id* p_id;
               add_tok(p_id = new id)->type = tk::id;
               p_id->str = buff;
            }
            buff.clear();
         }
         if (prew_state != state_t::sym) {
            buff.push_back(a);
         }
         else {
            add_sym_token(a);
         }
      }

   }
}


fn read_tokens(char const* path, std::vector<tok*>& output) -> void
{
   std::ifstream file(path);

   std::string solid_utf8_buffer(
      (std::istreambuf_iterator<char>(file)) // nu vot chto eto za govno ?
      , std::istreambuf_iterator<char>()
   );
   std::string almost_ascii_buffer;

   cyr_utf8_ids_to_ascii(solid_utf8_buffer, almost_ascii_buffer);

   pt = &output;
   tokenize(almost_ascii_buffer); // v output
   line = 0;
   pt = nullptr;

   std::cout << "\n" << almost_ascii_buffer << std::endl;
}


fn test_with(std::vector<tok*>& t, char const text_path[]) -> bool
{
   std::ifstream file(text_path);
   size_t idx = 0;

   while (!file.eof()) {
      if (t.size() == idx) break;
      std::string buff;
      file >> buff;
      tk tp = t[idx]->type;

      if (buff == "induc") { if (tp != tk::induc) return false; }
      else if (buff == "id") { if (tp != tk::id)  return false; }
      else if (buff == "num") { if (tp != tk::i32 && tp != tk::i64 && tp != tk::i128) return false; }
      else if (buff == "eol") { if (tp != tk::eol) return false; }
      else if (buff == "dot") { if (tp != tk::dot) return false; }
      else if (buff == "if") { if (tp != tk::npu) return false; }
      else if (buff == "else") { if (tp != tk::uHa) return false; }
      else if (buff == "ret") { if (tp != tk::rn) return false; }
      else if (buff == "struc") { if (tp != tk::struc) return false; }
      else if (buff == "str") { if (tp != tk::str) return false; }
      else if (buff == "fun") { if (tp != tk::fun) return false; }
      else if (buff == "opn") { if (tp != tk::opn) return false; }
      else if (buff == "cls") { if (tp != tk::cls) return false; }
      else if (buff == "more") { if (tp != tk::mor) return false; }
      else if (buff == "less") { if (tp != tk::les) return false; }
      else if (buff == "eq") { if (tp != tk::eq) return false; }
      else if (buff == "or") { if (tp != tk::unu) return false; }
      else if (buff == "and") { if (tp != tk::u) return false; }
      else if (buff == "not") { if (tp != tk::He) return false; }
      else if (buff == "$") { if (tp != tk::dlr) return false; }
      else if (buff == "#") { if (tp != tk::hsh) return false; }
      else if (buff == "@") { if (tp != tk::dog) return false; }
      else if (buff == ",") { if (tp != tk::sem) return false; }
      else if (buff == ":") { if (tp != tk::col) return false; }
      else if (buff == "+") { if (tp != tk::add) return false; }
      else if (buff == "*") { if (tp != tk::mul) return false; }
      else if (buff == "-") { if (tp != tk::sub) return false; }
      else if (buff == "/") { if (tp != tk::sep) return false; }
      else if (buff == "elif") { if (tp != tk::uHanpu) return false; }
      else if (buff == "break") { if (tp != tk::br) return false; }
      else if (buff == "continue") { if (tp != tk::cnt) return false; }
      else if (buff == "while") { if (tp != tk::whl) return false; }
      else if (buff == "for") { if (tp != tk::forch) return false; }
      else if (buff == "end") { if (tp != tk::cls_body) return false; }
      ++idx;
   }

   return true;
}
